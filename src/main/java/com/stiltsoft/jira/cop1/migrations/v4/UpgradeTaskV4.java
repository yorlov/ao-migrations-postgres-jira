package com.stiltsoft.jira.cop1.migrations.v4;

import com.stiltsoft.jira.cop1.migrations.AbstractMigrationTask;
import com.stiltsoft.jira.cop1.migrations.Versions;

public class UpgradeTaskV4 extends AbstractMigrationTask {

    public UpgradeTaskV4() {
        super(Versions.V3, Versions.V4, AoTaskV4.class);
    }
}