package com.stiltsoft.jira.cop1.migrations;

public class Versions {
    public static final int V0 = 0;
    public static final int V1 = V0 + 1;
    public static final int V2 = V1 + 1;
    public static final int V3 = V2 + 1;
    public static final int V4 = V3 + 1;
    public static final int V5 = V4 + 1;
    public static final int V6 = V5 + 1;
}