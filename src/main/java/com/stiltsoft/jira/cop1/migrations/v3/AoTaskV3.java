package com.stiltsoft.jira.cop1.migrations.v3;

import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

import static com.stiltsoft.jira.cop1.AoTask.TABLE_NAME;

@Table(TABLE_NAME)
public interface AoTaskV3 extends Entity {

    @Indexed
    long getProject();

    long getCreatedAt();

    String getV3();
}