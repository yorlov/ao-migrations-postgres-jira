package com.stiltsoft.jira.cop1.migrations.v6;

import com.stiltsoft.jira.cop1.migrations.AbstractMigrationTask;
import com.stiltsoft.jira.cop1.migrations.Versions;

public class UpgradeTaskV6 extends AbstractMigrationTask {

    public UpgradeTaskV6() {
        super(Versions.V5, Versions.V6, AoTaskV6.class);
    }
}