package com.stiltsoft.jira.cop1;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TasksScheduler implements InitializingBean, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(TasksScheduler.class);

    private TasksService tasksService;
    private ProjectManager projectManager;
    private EventPublisher eventPublisher;
    private TasksExecutor executor;

    @Autowired
    public TasksScheduler(TasksService tasksService,
                          ProjectManager projectManager,
                          TasksExecutor executor,
                          @ComponentImport EventPublisher eventPublisher) {
        this.tasksService = tasksService;
        this.projectManager = projectManager;
        this.eventPublisher = eventPublisher;
        this.executor = executor;
    }

    @EventListener
    public void onPluginEnabled(PluginEnabledEvent event) {
        if (event.getPlugin().getKey().equals("com.stiltsoft.jira.cop1")) {
            log.warn("Enjoy!");
            executor.execute(() -> projectManager.getProjects().forEach(project -> {
                tasksService.schedule(project);
                log.warn("Scheduled {}", project);
            }));
        }
    }

    @Override
    public void afterPropertiesSet() {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() {
        eventPublisher.unregister(this);
    }
}