package com.stiltsoft.jira.cop1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static java.util.concurrent.Executors.newFixedThreadPool;

@Component
public class TasksExecutor implements Executor, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(TasksExecutor.class);

    private ExecutorService executor;

    public TasksExecutor() {
        executor = newFixedThreadPool(1, namedThreadFactory("cop1"));
    }

    @Override
    public void execute(@Nonnull Runnable runnable) {
        executor.execute(() -> {
            try {
                runnable.run();
            } catch (Exception e) {
                log.warn("Houston, we have a problem", e);
            }
        });
    }

    @Override
    public void destroy() {
        executor.shutdown();
    }
}