package com.stiltsoft.jira.cop1;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.stiltsoft.jira.cop1.AoTask.CREATED_AT_COLUMN;
import static com.stiltsoft.jira.cop1.AoTask.PROJECT_COLUMN;
import static java.lang.System.currentTimeMillis;

@Component
public class TasksDao {

    private ActiveObjects ao;

    @Autowired
    public TasksDao(@ComponentImport ActiveObjects ao) {
        this.ao = ao;
    }

    public AoTask createTask(long project) {
        return ao.create(AoTask.class, ImmutableMap.of(
                PROJECT_COLUMN, project,
                CREATED_AT_COLUMN, currentTimeMillis()
        ));
    }
}