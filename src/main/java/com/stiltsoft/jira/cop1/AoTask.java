package com.stiltsoft.jira.cop1;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

import static com.stiltsoft.jira.cop1.AoTask.TABLE_NAME;

@Table(TABLE_NAME)
public interface AoTask extends Entity {

    String TABLE_NAME = "TASKS";
    String PROJECT_COLUMN = "PROJECT";
    String CREATED_AT_COLUMN = "CREATED_AT";

    @Accessor(PROJECT_COLUMN)
    @Indexed
    long getProject();

    @Accessor(CREATED_AT_COLUMN)
    long getCreatedAt();

}